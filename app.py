import random
import os
import requests
import re
from flask import Flask, render_template
from urllib.parse import urlparse

app = Flask(__name__)

HITS = 0

@app.route('/')
def index():
	global HITS
	HITS = HITS + 1
	msg = f'This webpage has been viewed {HITS} times'
	return msg

@app.route("/rolldice")
def roll_dice():
	result = do_roll()
	return result

def do_roll():
	r = str(random.randint(1, 6))
	return r

@app.route('/doggo')
def fetch_dog():
    resp = requests.get("https://dog.ceo/api/breeds/image/random")
    if resp.status_code > 300:
        return "Error!"
    else: 
        image_src = resp.json()["message"]
        breed = get_breed(image_src)
        return render_template('random-pet-pic.html', img_src=image_src, breed=breed)
    return 

def get_breed(url): 
	path = urlparse(url).path
	match = re.search(r"/breeds/([^/]+)/", path)
	if match:
		result = match.group(1)
		return result

if __name__ == "__main__":
	app.run(host="0.0.0.0")
